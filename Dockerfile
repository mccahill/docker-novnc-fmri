
FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

############ core novnc install ###############
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    locales-all \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get update   &&  apt-get dist-upgrade -y

RUN apt-get install -y --force-yes --no-install-recommends \
        python-numpy \
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        x11vnc \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        libpci-dev \
        xserver-xorg-video-dummy \
        libglu1 \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

ADD startup.sh /
ADD cleanup-cruft.sh /
ADD initialize.sh /

ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
ADD noVNC /noVNC/
ADD noVNC_cert /noVNC_cert/
# make sure the noVNC cert files are  only readable by root
RUN  chmod 400 /noVNC_cert/self.*

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf

############ end core novnc install ###############


############ begin helpful packages ###############
RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 zip \
 unzip \
 git \
 texlive-latex-base \
 texlive-latex-extra \
 texlive-fonts-extra \
 texlive-fonts-recommended \
 texlive-generic-recommended \
 libxrender1 \
 inkscape \
 zip \
 unzip \
 git \
 python3-pyqt5 \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*
 
############ end helpful packages ###############


##########################################
# MRI workshop-specific items:
#
# directory that holds BIAC, mricron_lx,  spm12
ADD mri /mri/
#
# fsl-complete
#
RUN wget -O- http://neuro.debian.net/lists/bionic.us-nh.full | sudo tee /etc/apt/sources.list.d/neurodebian.sources.list
RUN apt-key adv --recv-keys --keyserver hkps://keyserver.ubuntu.com 0xA5D32F012649A5A9
RUN apt-get update
RUN apt-get install -y --force-yes fsl-core
#
#
#
RUN apt-get install -y --force-yes unzip
#
# qt4 is needed by medInria
RUN apt-get install -y --force-yes qt4-default libqt4-sql-sqlite
#
##########################################
#
# desktop file(s) for apps
ADD desktop-icons/matlab-mri.desktop /usr/share/applications/matlab-mri.desktop
ADD desktop-icons/mricron.desktop /usr/share/applications/mricron.desktop
ADD desktop-icons/fsl.desktop /usr/share/applications/fsl.desktop
ADD desktop-icons/medinria.desktop /usr/share/applications/medinria.desktop

USER root

ENTRYPOINT ["/startup.sh"]


